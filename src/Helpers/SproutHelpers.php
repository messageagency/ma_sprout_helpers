<?php

namespace Drupal\ma_sprout_helpers\Helpers;

use Drupal\Core\Entity\EntityInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\Entity\Media;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

class SproutHelpers {
  /**
   * Get the focal point coordinates from and image item.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $item
   *
   * @return array
   *
   * TODO: Stress test this.
   */
  public static function getFocalPointAnchor($item) {
    if (!$item instanceof ImageItem) {
      return [];
    }
    $file = $item->entity;
    $crop_type = \Drupal::config('focal_point.settings')->get('crop_type');
    $crop = \Drupal::service('focal_point.manager')
      ->getCropEntity($file, $crop_type);
    $anchor = \Drupal::service('focal_point.manager')
      ->absoluteToRelative($crop->x->value, $crop->y->value, $item->width, $item->height);

    return $anchor;
  }

  /**
   * Given an entity and an image field, return image info.
   *
   * TODO: this could probably use some stress testing.
   *
   * @param object $entity
   *   An entity with an image field.
   * @param string $field_name
   *   The name of the image field.
   * @param string $style
   *   Optional image style name.
   *
   * @return array containing:
   *   - uri: URI of the image.
   *   - url: URL of the image.
   *   - path: Relative path of the image.
   *   - width: Original image width.
   *   - height: Original image height.
   *   - render: Render array of the image.
   *   - style: Array of the image info with the given style applied.
   */
  public static function getImageInfo(EntityInterface $entity, $field_name, $style = 'original') {
    $img_info = [];
    if (!$entity->hasField($field_name)) {
      return $img_info;
    }

    $img_item = $entity->$field_name->first();
    if (empty($img_item)) {
      return $img_info;
    }

    $img_file = $img_item->entity;
    if ($img_file instanceof Media) {
      if ($img_file->hasField('field_media_image') && !$img_file->field_media_image->isEmpty()) {
        $img_item = $img_file->field_media_image->first();
        $img_file = $img_item->entity;
      }
    }
    if (!$img_file instanceof File) {
      return $img_info;
    }

    $uri = $img_file->getFileUri();
    $url = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
    /** @var \Drupal\Core\Image\Image $image */
    $image = \Drupal::service('image.factory')->get($uri);
    if ($image->isValid()) {
      $width = $image->getWidth();
      $height = $image->getHeight();
    }
    else {
      $width = $height = NULL;
    }
    $anchor = self::getFocalPointAnchor($img_item);

    $img_info = [
      'uri' => $uri,
      'url' => $url,
      'path' => \Drupal::service('file_url_generator')->transformRelative($url),
      'width' => $width,
      'height' => $height,
      'anchor' => $anchor,
      'render' => [
        '#theme' => 'image',
        '#uri' => $uri,
      ],
      'style' => [],
    ];
    if ($style !== 'original') {
      $style_obj = ImageStyle::load($style);
      if (!$style_obj) {
        $style_obj = ResponsiveImageStyle::load($style);
      }
      if ($style_obj instanceof ImageStyle) {
        // more than one effect? don't care.
        $effects_config = $style_obj->getEffects()->getConfiguration();
        foreach ($effects_config as $config) {
          $width = $config['data']['width'];
          $height = $config['data']['height'];
        }
        $img_info['style'] = [
          'name' => $style,
          'url' => $style_obj->buildUrl($uri),
          'width' => $width,
          'height' => $height,
          'anchor' => $anchor,
          'render' => [
            '#theme' => 'image_style',
            '#style_name' => $style,
            '#uri' => $uri,
            '#width' => $width,
            '#height' => $height,
          ],
        ];
      }
      if ($style_obj instanceof ResponsiveImageStyle) {
        $img_info['style'] = [
          'name' => $style,
          //'url' => $style_obj->buildUrl($uri),
          'width' => $width,
          'height' => $height,
          'anchor' => $anchor,
          'render' => [
            '#theme' => 'responsive_image',
            '#responsive_image_style_id' => $style,
            '#uri' => $uri,
            '#width' => $width,
            '#height' => $height,
          ],
        ];
      }
    }

    return $img_info;
  }

  /**
   * I ganked this from blockLabel() in menu_block/src/Plugin/Block/menuBlock.php.
   * I'm not a big fan of this - kosbob
   *
   * @param string $pluginId
   *   Name of plugin.
   *
   * @return string
   */
  public static function getMenuLinkParentLabel($pluginId) {
    if (strpos($pluginId, 'menu_link_content:') === FALSE) {
      return;
    }
    list($entityType, $entityUuid) = explode(':', $pluginId);
    $menu_link_ids = \Drupal::entityQuery($entityType)
      ->accessCheck(FALSE)
      ->condition('uuid', $entityUuid)
      ->execute();
    if ($menu_link_ids) {
      $menu_link_id = reset($menu_link_ids);
      $menu_link = MenuLinkContent::load($menu_link_id);
      $parent_link = $menu_link->get('parent')->value;
      return self::getMenuLinkLabel($parent_link);
    }
    return '';
  }

  /**
   * @see \Drupal\ma_sprout\Helpers\SproutHelpers::getMenuLinkParentLabel().
   *
   * @param string $pluginId
   *   Name of plugin.
   *
   * @return string
   */
  public static function getMenuLinkLabel($pluginId) {
    if (!$pluginId || strpos($pluginId, 'menu_link_content:') === FALSE) {
      return;
    }
    list($entityType, $entityUuid) = explode(':', $pluginId);
    $menu_link_ids = \Drupal::entityQuery($entityType)
      ->accessCheck(FALSE)
      ->condition('uuid', $entityUuid)
      ->execute();
    if ($menu_link_ids) {
      $menu_link_id = reset($menu_link_ids);
      $menu_link = MenuLinkContent::load($menu_link_id);
      return $menu_link->getTitle();
    }
    return '';
  }

  /**
   * Unset multiple subnav menus. Precedence is main->secondary->supplemental.
   *
   * DEPRECATED. Use SingleField() instead.
   */
  public static function setDsNavPrecedence(&$variables) {
    // Main menu subnav
    if (isset($variables['content']['dynamic_block_field:node-subnav'])) {
      if (isset($variables['content']['dynamic_block_field:node-subnav_secondary'])) {
        unset($variables['content']['dynamic_block_field:node-subnav_secondary']);
        unset($variables['region_attributes']['dynamic_block_field:node-subnav_secondary']);
      }
      if (isset($variables['content']['dynamic_block_field:node-subnav_supplemental'])) {
        unset($variables['content']['dynamic_block_field:node-subnav_supplemental']);
        unset($variables['region_attributes']['dynamic_block_field:node-subnav_supplemental']);
      }
    }
    // Secondary menu subnav
    if (isset($variables['content']['dynamic_block_field:node-subnav_secondary'])) {
      if (isset($variables['content']['dynamic_block_field:node-subnav_supplemental'])) {
        unset($variables['content']['dynamic_block_field:node-subnav_supplemental']);
        unset($variables['region_attributes']['dynamic_block_field:node-subnav_supplemental']);
      }
    }

    return $variables;
  }

  /**
   * Remove a field from the content array dependent of the presence of another field.
   *
   * @param array $content
   *   Content array from $variables.
   * @param string $field_keep
   *   Name of field that should be kept.
   * @param string $field_unset
   *   Name of field that should be removed if the kept field is present.
   */
  public static function SingleField(&$content, $field_keep, $field_unset) {
    if (isset($content[$field_keep]) && isset($content[$field_keep]['#items'])) {
      if (isset($content[$field_unset])) {
        unset($content[$field_unset]);
        if (isset($content['#ds_configuration'])) {
          $region = '';
          foreach ($content['#ds_configuration']['regions'] as $region => $fields) {
            if (in_array($field_unset, $fields)) {
              break;
            }
          }
          if ($region) {
            unset($content[$region][$field_unset]);
          }
        }
      }
    }
  }

}
